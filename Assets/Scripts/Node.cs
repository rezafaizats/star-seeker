﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public bool isWalkable;
    public Vector3 worldPos;
    
    public int gCost;
    public int hCost;

    public int gridX;
    public int gridY;
    public Node parent;

    public Node(bool _isWalkable, Vector3 _worldPos, int _gridX, int _gridY)
    {
        isWalkable = _isWalkable;
        worldPos = _worldPos;
        gridX = _gridX;
        gridY = _gridY;
    }

    public int fCost()
    {
        return gCost + hCost;
    }

}
